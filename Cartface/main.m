//
//  main.m
//  Cartface
//
//  Created by Aditi on 11/05/16.
//  Copyright © 2016 Aditi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
