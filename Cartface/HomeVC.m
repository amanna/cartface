//
//  HomeVC.m
//  Cartface
//
//  Created by Aditi on 11/05/16.
//  Copyright © 2016 Aditi. All rights reserved.
//

#import "HomeVC.h"

@interface HomeVC ()
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
- (IBAction)btnSignupAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnLoginAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *myView;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;

@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UILabel *lbl3;

@property (weak, nonatomic) IBOutlet UILabel *lbl4;
@property (weak, nonatomic) IBOutlet UILabel *lbl5;
@property (weak, nonatomic) IBOutlet UILabel *lbl6;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl6Leading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl1Leading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl2leading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl3leading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl4leading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl5leading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *htConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation HomeVC
-(void)textAnimation{
    [UIView animateWithDuration:0.9 delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
        _lbl1Leading.constant=(self.view.frame.size.width - self.lbl1.frame.size.width)/2;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.9 delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
            _lbl2leading.constant=(self.view.frame.size.width - self.lbl2.frame.size.width)/2;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.9 delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
                _lbl3leading.constant=(self.view.frame.size.width - self.lbl3.frame.size.width)/2;
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.9 delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    _lbl4leading.constant=(self.view.frame.size.width - self.lbl4.frame.size.width)/2;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.9 delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        _lbl5leading.constant=(self.view.frame.size.width - self.lbl5.frame.size.width)/2;
                        [self.view layoutIfNeeded];
                    } completion:^(BOOL finished) {
                        
                        [UIView animateWithDuration:0.9 delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
                            _lbl6Leading.constant=(self.view.frame.size.width - self.lbl6.frame.size.width)/2;
                            [self.view layoutIfNeeded];
                        } completion:^(BOOL finished) {
                            
                            
                        }];
                        
                    }];
                    
                }];
                
                
            }];
            
            
        }];
    }];

}
- (void)viewDidAppear:(BOOL)animated{
   // [self textAnimation];
    self.htConstraint.constant= (61 * 320)/414;
   self.topConstraint.constant= (self.view.frame.size.height - 61)/2;
     [self.view layoutIfNeeded];
 
  [UIView animateWithDuration:0.9 delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
      self.leadingConst.constant= (self.view.frame.size.width - 290)/2;
       [self.view layoutIfNeeded];
  } completion:^(BOOL finished){
      
      [UIView animateWithDuration:0.9 delay:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
         self.topConstraint.constant= 18;
           [self.view layoutIfNeeded];
      } completion:^(BOOL finished) {
          
      }];
  }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSignupAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"segueSignUp" sender:self];
}
- (IBAction)btnLoginAction:(UIButton *)sender {
     [self performSegueWithIdentifier:@"segueLogin" sender:self];
}
@end
