//
//  SignupVC.m
//  Cartface
//
//  Created by Aditi on 11/05/16.
//  Copyright © 2016 Aditi. All rights reserved.
//

#import "SignupVC.h"

@interface SignupVC ()
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPass;

@property (weak, nonatomic) IBOutlet UITextField *txtStore;
@property (weak, nonatomic) IBOutlet UITextField *txtFname;

@property (weak, nonatomic) IBOutlet UITextField *txtLname;

@end

@implementation SignupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
